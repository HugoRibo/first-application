var datas = {

	frais : {

		1 : {
      vehicle_id : 1,
			type : 0,
			user_id : 1,
			date : "2017-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "2 rue Péclet 59300 Valenciennes",
			km : 0.5,
			description : "Visite partenaires"
		},

		2 : {
			vehicle_id : 3,
			type : 0,
			user_id : 2,
			date : "2017-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "15 rue de POPSCHOOL 59000 Lille",
			km : 50,
			description : "Réunion"
		},

		3 : {
			vehicle_id : 1,
			type : 0,
			user_id : 1,
			date : "2017-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "78 rue de la gare 7500 Paris",
			km : 200,
			description : "Visite de stage"
		},

    4 : {
			vehicle_id : 3,
			type : 1,
			user_id : 2,
			date : "2017-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "9 rue des mouettes 59770 Marly",
			km : 78,
			description : "Beer Fest"
		},

		5 : {
			vehicle_id : 1,
			type : 1,
			user_id : 1,
			date : "2012-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "9 rue des mouettes 59770 Marly",
			km : 223,
			description : "Beer Fest"
		},

		6 : {
			vehicle_id : 4,
			type : 0,
			user_id : 3,
			date : "2013-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "9 rue des mouettes 59770 Marly",
			km : 36,
			description : "Beer Fest"
		},

		7 : {
			vehicle_id : 4,
			type : 0,
			user_id : 3,
			date : "2013-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "9 rue des mouettes 59770 Marly",
			km : 420,
			description : "Beer Fest"
		},

		8 : {
			vehicle_id : 2,
			type : 0,
			user_id : 2,
			date : "2013-01-01",
			start : "80 Avenue Roland Moreno 59410 Anzin",
			end : "9 rue des mouettes 59770 Marly",
			km : 78,
			description : "Beer Fest"
		}
	},

	cars : {
		1 : {
			name : "Citröen C3",
			matriculation : "XX-746-98",
			cv : 3,
			km : 0,
			owner_id : 3
		},

		2 : {
			name : "Golf 4",
			matriculation : "XX-746-98",
			cv : 6,
			km : 0,
			owner_id : 2
		},

		3 : {
			name : "AUDI R8",
			matriculation : "HX-766-AB",
			cv : 11,
			km : 0,
			owner_id : 1
		},

		4 : {
			name : "Nissan Qashqaï",
			matriculation : "UI-746-98",
			cv : 6,
			km : 0,
			owner_id : 1
		}
	},

  motos : {

		1 : {
			name : "Kawasaki Nagasaki",
			matriculation : "HH-420-77",
			cv : 5,
			km : 0,
			owner_id : 3
		},

		2 : {
			name : "Honda Cross'amer",
			matriculation : "BB-420-77",
			cv : 6,
			km : 0,
			owner_id : 1
		},

		3 : {
			name : "Ya maha alatélé",
			matriculation : "II-420-77",
			cv : 3,
			km : 0,
			owner_id : 5
		},

		4 : {
			name : "Bay aime Double-V",
			matriculation : "AA-420-77",
			cv : 1,
			km : 0,
			owner_id : 2
		}

	},


	users : {

		1 : {
			lastname : "Popy",
			firstname : "Loic",
			birthdate : "1999-10-12 23:59",
			licencedate : "2016-10-12",
			address : "rue je sais pas",
			zip : "59300",
			city : "Valenciennes"
		},

		2 : {
			lastname : "Popychan",
			firstname : "Ethan",
			birthdate : "1998-03-12 18:20",
			licencedate : "2015-11-12",
			address : "rue je sais pas",
			zip : "59000",
			city : "Lille"
		},

		3 : {
			lastname : "Pomme",
			firstname : "Camille",
			birthdate : "1990-03-19 00:01",
			licencedate : "2010-11-12",
			address : "rue je sais pas",
			zip : "59200",
			city : "Tourcoing"
		}
	}
}


/*
	Parser les notes de frais pour afficher : date / personne (nom+prenom) / voiture utilisée (name + plaque) / départ & arrivé / km parcouru / montant à rembourser
	Si la voiture est détenue par une personne ayant moins de 25 ans, la case de la voiture doit être orange
	Si la personne qui a crée la note de frais est née en Mars, la case du prénom doit être en bleu
	Si le nombre de chevaux est supérieur à 4 CV alors la case de voiture doit être en gris clair

	Parser ensuite les voitures afin de lister les voitures avec : name / immatriculation / propriétaire (nom/prénom/date permis) / km parcouru

	CDC EVOLUTIF

	TRAVAIL EN SOLO AVEC COMME DEADLINE à voir ce soir :)
 */

$(function(){

      $.each(datas.frais, function(id, d){

          date = d.date
          user_id = d.user_id

					birthdate = datas["users"][user_id]["birthdate"]
					lastname = datas["users"][user_id]["lastname"]
          firstname = datas["users"][user_id]["firstname"]
					type = d.type
					vehicle_id = d.vehicle_id
					start = d.start
					end = d.end
					km = d.km

					i = 0

					if(type == 0){
						vehiclename = datas["cars"][vehicle_id]["name"]
	          vehiclematricule = datas["cars"][vehicle_id]["matriculation"]
						cv = datas["cars"][vehicle_id]["cv"]
						kmparcouru = datas["cars"][vehicle_id]["km"] += d.km


						if(cv > 7){
							cv = 7
						}else if(cv < 3){
							cv = 3
						}
						var coeff = {

							3 : {
								1 : 0.41,
								2 : 0.245,
								3 : 0.286
							},
							4 : {
								1 : 0.493,
								2 : 0.277,
								3 : 0.332
							},
							5 : {
								1 : 0.543,
								2 : 0.305,
								3 : 0.364
							},
							6 : {
								1 : 0.568,
								2 : 0.32,
								3 : 0.382
							},
							7 : {
								1 : 0.595,
								2 : 0.337,
								3 : 0.401
							}
						}

						var addition = {
							3 : 824,
							4 : 1082,
							5 : 1188,
							6 : 1244,
							7 : 1288
						}

					}else if(type == 1){
						vehiclename = datas["motos"][vehicle_id]["name"]
	          vehiclematricule = datas["motos"][vehicle_id]["matriculation"]
						cv = datas["motos"][vehicle_id]["cv"]
						kmparcouru = datas["motos"][vehicle_id]["km"] += d.km

						if(cv == 2){
							cv = 1
						}else if(cv > 3 && cv < 6){
							cv = 3
						}else if(cv > 5){
							cv = 6
						}

						var coeff = {

							1 : {
								1 : 0.338,
								2 : 0.084,
								3 : 0.211
							},

							3 : {
								1 : 0.4,
								2 : 0.07,
								3 : 0.235
							},

							5 : {
								1 : 0.518,
								2 : 0.067,
								3 : 0.292
							}
					}

					var addition = {
						1 : 760,
						3 : 989,
						5 : 1351
					}
				}


					if(km > 0 && km < 5001){
			      i = 1
			    }else if(km > 5000 && km < 20001){
						i = 2
					}else if(km > 20001){
						i = 3
					}


					total = km * coeff[cv][i]

					if(km > 5000 && km < 20001){
						total = total + addition[cv]
					}


		var date2 = new Date();
   var date1 = new Date(birthdate);


   var period = date2.getFullYear() - date1.getFullYear();
   if (date2.getMonth() < date1.getMonth() - 1){
       period--;
   }
   if (date1.getMonth() - 1 == date2.getMonth() && date2.getDate() < date1.getDate()){
       period--;
   };

	 if(period < 25 && date1.getMonth() == 2){
		 collastfirstname = "<td class='rouge'>" + lastname + " " + firstname + "</td>"
	 }else if(period < 25){
		 collastfirstname = "<td class='orange'>" + lastname + " " + firstname + "</td>"
	 }else if(date1.getMonth() == 2){
		 collastfirstname = "<td class='grey'>" + lastname + " " + firstname + "</td>"
	 }else{
		 collastfirstname = "<td>" + lastname + " " + firstname + "</td>"
	 }

	 if(cv > 4){
		 colvehicule = "<td class='bleu'>" + vehiclename + "</td>"
	 }else{
		 colvehicule = "<td>" + vehiclename + "</td>"
	 }


          $("#table1").append("<tr>" + collastfirstname + colvehicule + "<td>" + vehiclematricule + "</td>" + "<td>" + km + "</td>" + "<td>" + total.toFixed(2) + "</td>" + "</tr>")

      })


			/*Parser ensuite les voitures afin de lister les voitures avec : name / immatriculation / propriétaire (nom/prénom/date permis) / km parcouru*/

			$.each(datas.cars, function(h, r){
						cars = r.name
						mat = r.matriculation
						owner_id = r.owner_id
						propname = datas["users"][owner_id]["lastname"]
						propfirstname = datas["users"][owner_id]["firstname"]
						licencedate = datas["users"][owner_id]["licencedate"]
						km = r.km

						totalkm = km


						$("#table2").append("<tr>" + "<td>" + cars + "</td>" + "<td>" + mat + "</td>" + "<td>" + propname + "</td>" +  "<td>" + propfirstname + "</td>" + "<td>" + licencedate + "</td>" + "<td>" + totalkm + "</td>" + "</tr>")


			})


})
